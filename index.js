/*Comando para levantar o servidor HTTP na porta 3000
const {createServer} = require('http');
const PORT = 3000;
require faz a requisição 
PORT é a variavel que vai ter a porta 3000
*/
const {createServer} = require('http');
const PORT = 3000;


let server = createServer(
    (req, resp) => {
        resp.writeHead(200,{"content-type" : "text/html"});
        resp.write("<h1>Hello Word: turma da PTA</h1>");
        resp.end();
    }
);


server.listen(PORT);
console.log(`Projeto iniciado na porta ${PORT}`);